from pyinfra import host
from pyinfra.api import deploy
from pyinfra.operations import dnf
from pyinfra.facts.server import LinuxDistribution


@deploy("install RPM fusion")
def install():
    fedora_release = host.get_fact(LinuxDistribution)["major"]
    free_url = f"https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-{fedora_release}.noarch.rpm"
    nonfree_url = f"https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-{fedora_release}.noarch.rpm"
    dnf.rpm(free_url)
    dnf.rpm(nonfree_url)
