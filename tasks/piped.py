from pyinfra import host
from pyinfra.api import deploy
from pyinfra.operations import files, server, dnf, postgres, lxd
from pyinfra.facts.server import Which
from pyinfra.facts.lxd import LxdContainers

from .operations.git import repo
from .postgresql import install as install_postgresql
from .pleroma import WithSecrets
from .install_consul_server import template_and_install_systemd
from .yts import lxc_shell


@deploy(
    "create lxc container that'll run piped",
    data_defaults={
        "piped_container_name": "piped",
        "piped_container_image": "images:fedora/38",
    },
)
def install_lxc_container():
    containers = host.get_fact(LxdContainers)
    ct_name = host.data.piped_container_name

    found_piped_container = False
    for container in containers:
        if container["name"] == ct_name:
            found_piped_container = True

    if not found_piped_container:
        lxd.container(
            name="create piped container",
            id=ct_name,
            image=host.data.piped_container_image,
        )

    # validate the ct is good
    lxc_shell(ct_name, "env")


@deploy("install piped backend", data_defaults={"piped_proxy": None})
def install():
    install_postgresql()
    dnf.packages(
        [
            "java-21-openjdk-headless",
        ]
    )

    with_secrets = WithSecrets(("piped_db_password",))

    # TODO remove copypaste of this between piped and pleroma
    has_postgres = host.get_fact(Which, command="psql")
    postgres_kwargs = {}
    if has_postgres:
        postgres_kwargs = {"_sudo": True, "_sudo_user": "postgres"}

    postgres.role(
        role=host.data.piped_db_user,
        password=with_secrets.piped_db_password,
        login=True,
        **postgres_kwargs,
    )

    postgres.database(
        database=host.data.piped_db_name,
        owner=host.data.piped_db_user,
        encoding="UTF8",
        **postgres_kwargs,
    )

    runner_user = "piped"
    remote_main_home_path = f"/opt/piped"

    # we really dont need to build the whole thing
    # just croc the jar file manually

    server.group(runner_user)
    server.user(
        user=runner_user,
        present=True,
        home=remote_main_home_path,
        shell="/bin/false",
        group=runner_user,
        ensure_home=True,
    )

    config_output = files.template(
        "./files/piped/config.properties",
        dest=f"{remote_main_home_path}/config.properties",
        user=runner_user,
        group=runner_user,
        mode=500,
        env_dict=with_secrets,
    )

    template_and_install_systemd(
        "./files/piped/piped.service.j2",
        env_dict={
            "user": runner_user,
            "remote_main_home_path": remote_main_home_path,
        },
    )
