from pathlib import Path
from packaging import version
from pyinfra.operations import apk, server, files
from pyinfra.facts.server import LinuxName
from pyinfra.api import deploy
from pyinfra import host

CROC_ALPINE_VERSION = version.parse("3.14")


@deploy(
    "install croc",
    data_defaults={
        "croc_install_directory": "/opt/croc",
        "croc_bin_directory": "/usr/bin",
    },
)
def install_croc():
    # alpine provides croc in-repo as of 3.14
    if host.get_fact(LinuxName) == "Alpine":
        host_alpine_version = version.parse(host.data.alpine_version)
        if host_alpine_version >= CROC_ALPINE_VERSION:
            apk.packages(name="install croc via apk", packages=["croc"])
            return

    # for everyone else, install manually
    croc_dir = Path(host.data.croc_install_directory)
    target_dir = host.data.croc_bin_directory
    files.directory(str(croc_dir))
    files.directory(target_dir)
    result = files.download(
        "https://github.com/schollz/croc/releases/download/v9.6.3/croc_9.6.3_Linux-64bit.tar.gz",
        str(croc_dir / "croc.tar.gz"),
        md5sum="5550b0bfb50d0541cba790562c180bd7",
    )
    if result.changed:
        server.shell(f"tar xvf {croc_dir}/croc.tar.gz", _chdir=str(croc_dir))
        server.shell(f"mv {croc_dir}/croc {target_dir}/croc", _chdir=str(croc_dir))
