from pyinfra import host
from pyinfra.operations import dnf, server, files, systemd, postgresql
from pyinfra.api import deploy
from pyinfra.facts.server import Which

from .operations.git import repo


@deploy("install aproxy")
def install():
    main_path = "/opt/aproxy"
    repo_output = repo(
        name="clone aproxy repo",
        src="https://gitdab.com/luna/aproxy",
        dest=f"{main_path}/src",
        branch="mistress",
    )

    config_directory = "/etc/aproxy"
    files.directory(config_directory)
    remote_config_path = f"{config_directory}/conf.lua"
    config_output = files.template(
        "./files/aproxy/conf.lua.j2",
        dest=remote_config_path,
        mode=555,
        cfg=host.data,
    )
