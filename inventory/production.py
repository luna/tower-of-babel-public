from tasks.nginx import NginxServiceEntry, TLSPreset

pleroma_hosts = [
    (
        "95.216.209.28",
        {
            "ssh_user": "root",
            "vpn_enabled": False,
            "postgresql_version": 15,
            "pleroma_user": "pleroma_fl4pm",
            "pleroma_name": "Pleroma/lun-4",
            "pleroma_email": "pladmin@l4.pm",
            "pleroma_description": "luna's instance",
            "pleroma_db_name": "pleroma_dev",
            "pleroma_db_user": "pleroma",
            "pleroma_host": "f.l4.pm",
            "nginx_image": "openresty/openresty:1.21.4.1-4-alpine",
            "nginx_config_folder": "/usr/local/openresty/nginx/conf/nginx.conf",
            "nginx_pcre_jit": True,
            "nginx_user": None,
            "nginx_mime_types_file": "mime.types",
            "nginx_lua_package_path": "/etc/lunar/?.lua;/etc/lunar/aproxy/?.lua;;",
            "nginx_lua_package_cpath": "/etc/lunar/?.lua;/etc/lunar/aproxy/?.lua;;",
            "nginx_init_by_lua_block": """
                require("aproxy.main").init()
            """,
            "nginx_services": {
                "f.l4.pm": NginxServiceEntry(
                    tls=True,
                    tls_preset=TLSPreset.pleroma,
                    vpn=False,
                    internet=True,
                    proxy_pass="localhost:4000",
                    client_max_body_size="70m",
                    http_redirect=True,
                    lua_code_cache=True,
                    access_by_lua_block="""
                        require("aproxy.main").access()
                    """,
                )
            },
            "nginx_extra_volumes": [
                ("/opt/aproxy/src", "/etc/lunar/aproxy"),
                ("/etc/aproxy", "/etc/aproxy"),
            ],
            "aproxy_accounts": [
                "luna@f.l4.pm",
            ],
        },
    )
]


MONITORING_SERVICE = NginxServiceEntry(
    tls=True,
    tls_preset=TLSPreset.mozilla_intermediate,
    vpn=False,
    internet=True,
    proxy_pass="localhost:3001",
)


monitoring_hosts = [
    (
        "51.158.191.23",
        {
            "ssh_user": "root",
            "vpn_enabled": False,
            "uptime_kuma_bind_port": 3001,
            "nginx_allow_php": True,
            "nginx_services": {
                "status.l4.pm": MONITORING_SERVICE,
            },
        },
    )
]
