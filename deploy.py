import os
import tasks

from pyinfra import host, config
from pyinfra.facts.server import LinuxName, Which
from pyinfra.operations import server, apk, apt
from tasks.secrets import secrets
from tasks.croc import install_croc

config.REQUIRE_PACKAGES = "requirements.txt"


if LinuxName == "Alpine":
    # make sure template operations work smoothly
    # because they run under smtp
    apk.packages(
        name="add openssh sftp package for pyinfra file operations",
        packages=[
            "openssh-sftp-server",
        ],
    )

install_croc()


def main_operations():
    if LinuxName == "Ubuntu":
        if host.data.remove_ubuntu_snap:
            server.shell("snap remove lxd")
            server.shell("snap remove core20")
            server.shell("snap remove snapd")
            apt.purge(["snapd"])

    if "pleroma_hosts" in host.groups:
        tasks.pleroma.install()
        tasks.aproxy.install()
        tasks.nginx.install_with_services()

    if "static_file_hosts" in host.groups:
        tasks.nginx.install_with_services()

    if "monitoring_hosts" in host.groups:
        tasks.uptime_kuma.install()
        tasks.nginx.install_with_services()


# setting so that you can quickly iterate in a specific host
maybe_specific_group = os.environ.get("DEPLOY_GROUPS")
if maybe_specific_group:
    if maybe_specific_group in host.groups:
        main_operations()
else:
    main_operations()
